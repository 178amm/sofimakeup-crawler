'use strict';

/*
 * sofimakeup-crawler
 * Copyright(c) 2017 Adrian Martinez <adriandev.me@gmail.com>
 * MIT Licensed
 */

/**
 * Module dependencies
 */
const apppath = require('app-module-path').addPath(__dirname + '/node_modules_custom');
const redis = require('./node_modules_custom/redis-singleton-connection');
const mongodb = require('./node_modules_custom/mongodb-connection');
const config = require('./config');

const program = require('commander');
const logger = require('logger');
const async = require('async');

 /**
 * Bootstrap Mongoose models
 */
require ('./app/models/Product');
require ('./app/models/Notification');

 /**
 * Bootstrap App files
 */
const main = require('./app/main');
const pjson = require('./package.json');

/**
 * DDBB Connect
 */
async.parallel([
	//Connect to the REDIS server
	function(cb){redis.connect(config.ddbb.redis.url, cb)},
    //Connect to the MONGODB server
    function(cb){mongodb.connect(config.ddbb.mongodb.url, cb)},
//Once done, start running
], function(err, data){
	if (err)
		logger.error(err);
	else //Run
		run(function(err, data){
			//Run finished, disconnect
			logger.info('REDIS:', 'Disconnect..');
			redis.disconnect(function(){});
			logger.info('MONGODB:', 'Disconnect..');
			mongodb.disconnect(function(){});
		});
});


/**
 * Run
 */
function run(cb){
	logger.info('Running crawler...');
	logger.info('Enviroment:', process.env.NODE_ENV || 'development');


	// program
	//   .version(pjson.version)
	//   .option('-c, --category-pages <store>', 'Crawl category pages from a json file, stored at data/<store>.categories.json,', main.crawlCategoryPagesFromJSON)
	//   .option('-p, --product-pages <store>', 'Crawl product pages from category pages stored on the ddbb', main.crawlProductFromCategoryPages)
	//   .option('-P, --products <store>', 'Crawl product from product pages stored on the ddbb', main.crawlProductsPricesFromProductURLs)
	//   .parse(process.argv);

	// Category Pages crawler
	program
	  .command('category-pages <store>')
	  .alias('cpa')
	  .description('Crawl category pages from a json file, stored at data/<store>.categories.json')
	  .option("-f, --file-path <file>", "Specify the json file path")
	  .action(function(store, options){
	  	//
	  	logger.info('Crawling category pages...');
	  	main.crawlCategoryPagesFromJSON(store, options.filePath, function(err, data){
	  		logger.info('Crawling category pages done!');
	  		cb(err, data);
	  	});
	  });

	// Product pages crawler
	program
	  .command('product-pages <store>')
	  .alias('ppa')
	  .description('Crawl product pages from category pages stored on the ddbb')
	  //.option("-f, --file-path <file>", "Specify the json file path")
	  .action(function(store, options){
	  	//
	  	logger.info('Crawling product pages...');
	  	main.crawlProductFromCategoryPages(store, function(err, data){
	  		logger.info('Crawling product pages done!');
	  		cb(err, data);
	  	});
	  });

	// Products
	program
	  .command('product-prices <store>')
	  .alias('ppr')
	  .description('Crawl product pages from category pages stored on the ddbb')
	  .option("-m, --max <number>", "Maximum products to crawl (default all)")
	  .option("-b, --batch-size <number>", "Products to crawl per batch (default 1)")
	  .action(function(store, options){
	  	//
	  	logger.info('Crawling product prices...');
	  	main.crawlProductsPricesFromProductURLs(store, options, function(err, data){
	  		logger.info('Crawling product prices done!');
	  		cb(err, data);
	  	});
	  });

	program.parse(process.argv);
}

