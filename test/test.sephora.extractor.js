'use strict';

/*
 * sofimakeup-crawler
 * Copyright(c) 2017 Adrian Martinez <adriandev.me@gmail.com>
 * MIT Licensed
 */

/**
 * Module dependencies
 */
var path = require('path');
var apppath = require('app-module-path').addPath(path.resolve(__dirname, '../node_modules_custom'));
var logger = require('logger');
var Sephora = require('../app/extractors/Sephora');




/**
 * Run
 */

console.log('Running...');
var extractor = new Sephora();



// // Test 1
// extractor.getProductURLsFromCategoryPageURL('http://www.sephora.es/Maquillaje/Ojos/C343', function(err, data){
// 	if (err)
// 		console.log(err);
// 	else
// 		console.log(data);
// });

// Test 2
//var url = "http://www.sephora.es/Maquillaje/Paletas-y-Cofres/Ojos/Paleta-para-Ojos-Shade-Light-Eye-Contour/P2572024";
var url = "http://www.sephora.es/Maquillaje/Paletas-y-Cofres/Ojos/It-Palette-Paleta-de-Sombras-de-Ojos/P1485005";
extractor.getProductsFromProductURL(url, function(err, data){
	if (err)
		logger.error(err);
	//else
		//logger.debug(data);
});