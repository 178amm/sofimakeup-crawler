'use strict';

/*
 * sofimakeup-crawler
 * Copyright(c) 2017 Adrian Martinez <adriandev.me@gmail.com>
 * MIT Licensed
 */

/**
 * Module dependencies
 */
var Sephora = require('../app/pagers/Sephora');
var categories_list = require('./mocks/sephora.categories.json');
console.log(categories_list);



/**
 * Run
 */

console.log('Running...');
var pager = new Sephora();



// Start script
pager.getCategoryPageURLs('http://www.sephora.es/Maquillaje/Ojos/Lapices/C365', function(err, data){
	if (err)
		console.log(err);
	else
		console.log(data);
});
