/**
 * Expose
 */

module.exports = {
 	ddbb:{
 		redis:{
      		url: 'redis://test:test@localhost:6379/'
	   	},
	    mongodb:{
	    	url: 'mongodb://test:test@localhost:27017/sofimakeup-crawler'
	    }
  	},
	email: {      
		enabled: true,
		service: 'Gmail',
	  	auth: {
	    	user: 'mailtestnode@gmail.com',
	    	pass: '%Vectalia0'
		},
		to: '178amm@gmail.com',
		body: 'Esto es un correo de prueba',
		subject:'Servicio automatico de impresion',
	},
	notifications: {
		errors: true,
		new: true,
		minimal: true
	}
};
