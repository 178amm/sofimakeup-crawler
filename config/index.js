
/**
 * Module dependencies.
 */

var path = require('path');
var extend = require('util')._extend;

var development = require('./env/development');
//var staging = require('./env/staging');
//var test = require('./env/test');
var production = require('./env/production');

var defaults = {
  root: path.normalize(__dirname + '/..')
};

/**
 * Expose
 */

module.exports = {
  development: extend(development, defaults),
  production: extend(production, defaults)
  //test: extend(test, defaults),
  //production: extend(production, defaults)
}[process.env.NODE_ENV || 'development'];
