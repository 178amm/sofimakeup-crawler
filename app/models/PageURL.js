/**
 * Requires
 */
const redis = require('../../node_modules_custom/redis-singleton-connection');
const logger = require('logger');

/**
 * Constructor
 */
var PageURL = function(config) {
  // Vars
  this.config = config;
  this.TABLE_NAME = 'PageURLSet';
}

/**
 * Properties
 */
PageURL.prototype = {  

  //Adds a new PageURL
  add: function(url_s, store, cb){
    client = redis.getClient();
    if (url_s && store){
      client.sadd(this.TABLE_NAME+":"+store, url_s);
      cb(null, url_s);
    }
    else
        cb(new Error('REDIS: Error, undefined params'));
  },

  getAll: function(store, cb){
    client = redis.getClient();
    client.smembers(this.TABLE_NAME+":"+store, cb);
  }
};

/**
 * Exports
 */
module.exports = new PageURL();


/**
 * Privates
 */