/*
 * Module dependencies
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var logger = require('logger');

/**
 * User schema
 */

var ProductSchema = new Schema({
  brand: { type: String, required: true },
  product: { type: String, required: true },
  color: { type: String },
  size: { type: String },
  detail: { type: String },
  image_url: { type: String },
  links: [{ //Links to different stores
    store_id: { type: String },
    url: { type: String }, //, unique: true 
  }]
});

/**
 * Text indexes
 */

ProductSchema.index({
    'product': "text",
    'color': "text",
    'size': "text",
    'detail': "text",
}, {
    weights: {
      'product': 10,
      'color': 8,
      'size': 2,
      'detail': 4
    },
    name: "searchIndex"
});

/**
 * Schema pre's & post
 */

// ProductSchema.pre('save', function(next) {

//     next();
// });

/**
 * User plugin
 */

//UserSchema.plugin(userPlugin, {});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

 //@TODO block editing pass

/**
 * Methods
 */

ProductSchema.method({

  addLinks : function (data, cb){
    //@TODO parse link structure
    this.links.push({store_id: data.shop, url: data.url});
    this.save(cb)
    //cb(null);
  },

});

/**
 * Statics
 */

ProductSchema.static({

  searchSimilarOld : function (data, cb){
    //@TOCHANGE This is momentary
    this.findOne({color: data.color, product: data.product})  
      .exec(cb);
  },

  searchSimilar : function (data, cb){
    var MIN_SCORE = 15;
    var textsearch = data.product+' '+(data.detail||'')+' '+(data.color||''); 

    logger.debug('PRODUCT-M: Finding similar for ', data);
    logger.debug('PRODUCT-M: Textsearch: ', textsearch);

    this.findOne( 
        { brand: data.brand,  
          "links.store_id": { $nin: data.shop }, // Exclude same store products
          $text : { $search : textsearch } }, // Search by textsearch query
        { score : { $meta: "textScore" } } // Retrieve score & filters the best
    )
    .sort({ score : { $meta : 'textScore' } })
    .exec(function(err, returned){
      // Last filter, exclude if doesnt pass min score
      if (returned && returned._doc.score < MIN_SCORE)
        returned = null;

      cb(err, returned);
    });
      
  },

  // search : function (params, cb) {
  //   var query = {};
  //   var by = params.by;
  //   var text = params.text;

  //   //Add regexp to the query
  //   if (text && text.length)
  //     query[by] = new RegExp(text, 'i');

  //   this.find(query)
  //       .exec(cb);
  // }, 

  // update: function (document, cb){

  // }
});

/**
 * Register
 */

mongoose.model('Product', ProductSchema);
