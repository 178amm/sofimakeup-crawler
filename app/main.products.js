/*
 * Module dependencies.
 */
var fs = require('fs');
//var _ = require('underscore');
var pageURL = require('./models/PageURL');
var productURL = require('./models/ProductURL');
var productPrice = require('./models/ProductPrice');
var notificationHandler = require('./handlers/notification');
const path = require('path');
const logger = require('logger');
const uuidv4 = require('uuid/v4');
const async = require('async');
const mongoose = require('mongoose');
const moment = require('moment');

var Product = mongoose.model('Product');

/*
 * Exports
 */

exports.crawlProductsPricesFromProductURLs = function(parser_name, options, cb){

    //Parse options 
    var MAX = options.max | 0;
    var BATCH_SIZE = options.batchSize | 1;
    //Statistics
    var stats_total = 0;
    var stats_init_time = Date.now();

    if (!MAX)
        logger.info('MAIN-PRODUCT: Crawling all products in slices of '+BATCH_SIZE);
    else
        logger.info('MAIN-PRODUCT: Crawling '+MAX+' product(s) in '+BATCH_SIZE+' batch(es)');

    //Retrieve all urls
    productURL.getAll(parser_name, function(err, productURLs){
        if (err)
            console.log(err);
        else{   
            //Set max
            if(MAX){
                productURLs = productURLs.slice(0, MAX);
                logger.debug('MAIN-PRODUCTS: Arr sliced')
            }
            //Chunk in batches
            var productURLBatches = productURLs.chunk(BATCH_SIZE);
            logger.debug('MAIN-PRODUCTS: productURLBatches', productURLBatches);
            //Define extractors
            var Extractor = require('./extractors/'+private.capitalize(parser_name));
            var extractor = new Extractor();     
            //Begin, per each batch, wait till each batch finishes...
            async.eachOfSeries(productURLBatches, function(productURLBatch, key, cbpub){
                //Itearate through each product URL Batch
                var stats_total_batch = 0;
                var init_time_batch = Date.now();
                async.eachOf(productURLBatch, function(productURL, key, cbpu){
                    //Extract product data from URL
                    extractor.getProductsFromProductURL(productURL, function(err, products){
                        if (err){
                            notificationHandler.notifyError(err, productURL);
                            cbpu(null);
                        }
                        else{ //Iterate extracted products
                            //@TODO switch
                            exports.crawlProductPricesFromURL(productURL, parser_name, function(err, data){
                                if (err){
                                    notificationHandler.notifyError(err, productURL);
                                    logger.error(err);
                                }
                                else{
                                    stats_total_batch++;
                                    stats_total++;
                                }
                                //Never stop, skip all errors
                                cbpu(null, data);
                            });
                        }
                    });
                }, function(err, data){
                    logger.info('MAIN-PRODUCTS: Ended batch!, crawled '+stats_total_batch+' productURLs');
                    logger.info('MAIN-PRODUCTS: Batch crawled in', moment().diff(init_time_batch, 'seconds')+' seconds');
                    if (err){
                        notificationHandler.notifyError(err);
                        logger.error(err);
                    }
                    //Never stop, skip all errors
                    stats_total_batch=0;
                    cbpub(null, data);
                });
            }, function(err, data){
                logger.info('MAIN-PRODUCTS: Ended all batches, finished!, crawled '+stats_total+' productURLs in total');
                logger.info('MAIN-PRODUCTS: All batches crawled in', moment().diff(stats_init_time, 'seconds')+' seconds');
                if(err){
                    notificationHandler.notifyError(err);
                    logger.error(err);
                }                    
                //Never stop, skip all errors
                cb(null, data);
            });
        }
    });
}

exports.crawlProductPricesFromURL = function(url, parser_name, cb){
    
    var Extractor = require('./extractors/'+private.capitalize(parser_name));
    var extractor = new Extractor();     
    
    //Extract product data from URL
    extractor.getProductsFromProductURL(url, function(err, products){
        if (err)
            cb(err);
        else{ //Iterate extracted products
            async.each(products, function(product, cbp){
                logger.debug('Products extracted from url, length: ', products.length);
                //Get product_uuid by product_store_id
                logger.debug('Finding product price...');
                private.findProductPrice(product, function(err, product_price_found){
                    if (err)
                        cbp(err);
                    if (!product_price_found){
                        logger.debug('Product price not found, saving...');
                        //Save product price (registers new product if necessary)
                        private.saveProductPrice(product, function(err, product_price_saved){
                            if(err)
                                cbp(err);
                            else{//Check minimal
                                logger.debug('Product price saved ', product_price_saved);
                                private.checkMinimalPrice(product, product_price_saved, cbp);
                            }
                        });
                    }
                    //If found, just update product price table
                    else{
                        logger.debug('Product price found, updating...');
                        private.updateProductPrice(product, function(err, product_price_saved){
                            if (err)
                                cbp(err);
                            else{ //Check minimal
                                logger.debug('Product price saved ', product_price_saved);
                                private.checkMinimalPrice(product, product_price_saved, cbp);
                            }
                        });
                    }
                });
            }, function(err, data){
                logger.info('MAIN-PRODUCTS: Products crawled from URL', url);
                if (err)
                    logger.error(err);
                cb(err, data);
            });
        }
    });
}


/*
 * Privates
 */
var private = {}

private.capitalize = function (string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}

private.findProduct= function (product, cb){

}

private.findProductPrice = function (product, cb){
    productPrice.getProductUUID(product.product_store_id, function(err, product_uuid){
        if(err)
            cb(err);
        else if (!product_uuid)
            cb(null, null)
        else{
            product.product_uuid = product_uuid;
            cb(null, product);
        }
    });
}

private.saveProductPrice = function (product, cb){ // cb(err, product, product_price) //@LONGFUNC
    //New product price!, tries to find a product who matches
    logger.debug('Saving product price, searching similar match..');
    Product.searchSimilar(product, function(err, product_found){
        if (err)
            cb(err)
        else if (!product_found){
            logger.debug("Similar not found, creating new..");
            notificationHandler.notifyNew(product);
            product_m = new Product(product);
            product_m.links.push({store_id: product.shop, url: product.url});
            logger.debug('Product', product);
            logger.debug('Product_m', product_m);
            //Not found, simply save NEW product data and..
            product_m.save(function(err, product_saved){
                logger.debug('Product (is supposed to) saved', product_saved);
                if(err)
                    cb(err);
                else{//.. save product Price
                    productPrice.setProductUUID(product_saved._id, product.product_store_id, function(err, pp){
                        if (err)
                            cb(err);
                        else{
                            // .. save product price
                            product.product_uuid = product_saved._id;
                            logger.debug('Product price updated correctly', product);
                            productPrice.add(product, cb);
                        }
                    });
                } 
            });
        }
        else{ //Product found, just push new link to product & save product price and...
            logger.debug('Similar product found!, updating links...', product)
            product_found.addLinks(product, function(err, product_updated){
                if(err)
                    cb(err);
                else if (!product_updated)
                    cb(new Error('MAIN-PRODUCTS: Error product_updated not found'));
                else{ // .. save product Price
                    logger.debug('Links added!, updating...');
                    productPrice.setProductUUID(product_updated._id, product.product_store_id, function(err, pp){
                        if (err)
                            cb(err);
                        else{
                            // .. save product Price
                            product.product_uuid = product_updated._id;
                            logger.debug('Product price updated correctly', product);
                            productPrice.add(product, cb);
                        }
                    });
                }
            });
        } 
    });
}

private.updateProductPrice = function (product, cb){
    logger.debug('Updating product price..');
    if (!product)
        cb(new Error('MAIN-PRODUCT: Error, not product_price provided'));
    else
        productPrice.add(product, cb);
}

private.checkMinimalPrice = function(product, product_price, cb){
    logger.debug('MAIN-PRODUCT: Checking minimal price...', product.product_uuid);
    productPrice.get(product.product_uuid, function(err, data){
        if (err)
            cb(err)
        else if (!data)
            cb(new Error ('MAIN-PRODUCT: Product not found by minimal price'));
        else if (Object.keys(data).length == 1){ // Not minimal, product is new
            logger.debug('MAIN-PRODUCT: Not a min price, product is new', data);
            cb(null, null) 
        }
        else{
            logger.debug('MAIN-PRODUCT: Hash found for '+product.product_uuid, data);
            if(productPrice.checkIfLatestIsTheCheapest(data)){ // Check minimal price
                logger.info('MAIN-PRODUCT: Minimal price found!! for', product.product_uuid);
                notificationHandler.notifyMinimal(product, product_price);
                cb(null, true);
            }
            else{
                logger.debug('MAIN-PRODUCT: Not a min price');
                cb(null, false);
            }
        }
    });
}   



//Array property of chunk
Object.defineProperty(Array.prototype, 'chunk', {
    value: function(chunkSize) {
        var array=this;
        return [].concat.apply([],
            array.map(function(elem,i) {
                return i%chunkSize ? [] : [array.slice(i,i+chunkSize)];
            })
        );
    }
});
















// AFTER @TODO Switch

                        // async.each(products, function(product, cbp){
                        //     logger.debug('Products extracted from url, length: ', products.length);
                        //     //Get product_uuid by product_store_id
                        //     logger.debug('Finding product price...');
                        //     private.findProductPrice(product, function(err, product_price_found){
                        //         if (err)
                        //             cbp(err);
                        //         if (!product_price_found){
                        //             logger.debug('Product price not found, saving...');
                        //             //Save product price (registers new product if necessary)
                        //             private.saveProductPrice(product, function(err, product_price_saved){
                        //                 if(err)
                        //                     cbp(err);
                        //                 else//Check minimal
                        //                     private.checkMinimalPrice(product.product_uuid, cbp);
                        //             });
                        //         }
                        //         //If found, just update product price table
                        //         else{
                        //             logger.debug('Product price found, updating...');
                        //             private.updateProductPrice(product, function(err, product_price_saved){
                        //                 if (err)
                        //                     cbp(err);
                        //                 else //Check minimal
                        //                     private.checkMinimalPrice(product.product_uuid, cbp);
                        //             });
                        //         }
                        //     });
                        // }, function(err, data){
                        //     logger.info('Ended cycle of product!');
                        //     if (err)
                        //         logger.error(err);
                        //     cbpu(err, data);
                        // });