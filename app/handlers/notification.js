/*
 * Module dependencies.
 */
var mongoose = require('mongoose');
var logger = require('logger')

var Notification = mongoose.model('Notification');

var config = require('../../config').notifications;
config.email = require('../../config').email;


/*
 * Exports
 */

exports.notifyMinimal = function(product, product_price){

    if (config.minimal){
        logger.info('NOTIFICATION-HANDLER: Minimal price found for', product);

        var notification = new Notification({
            data: product,
            type: 'app:min_price',
            text: 'Price downgrade to ' + product_price
        });

        notification.save(private.handleMail);
    }
}

exports.notifyNew = function(product){
    
    if (config.new){
        logger.info('NOTIFICATION-HANDLER: New product found!', product);

        var notification = new Notification({
            data: product,
            type: 'app:new_product',
            text: 'New product found with product_store_id ' + product.product_store_id
        });

        notification.save(private.handleMail);
    }
}

//Deatached
exports.notifyError = function(err, data){

    if (config.error){
        //Parse error notifications
        var notification;
        if (err){
            if (err.code == 'ETIMEDOUT'){
                notification = new Notification({
                    data: err,
                    type: 'error:timed_out',
                    text: data
                });
            }        
            //@TODO should consider every possible err 
            else{
                notification = new Notification({
                    value: JSON.stringify(err),
                    type: 'unknown',
                    //text: 'Unknown error'
                });
            }
        }

        if(notification)
            notification.save(private.handleMail);
    }
}

/*
 * Exports
 */

 var private = {};

 private.handleMail = function(err, data){
    if (err)
        logger.error('NOTIFICATION-HANDLER: Error, couldnt save notification', err);
    else
        if (config.email.enabled){
            //Sends an email with notification data
        }
 }