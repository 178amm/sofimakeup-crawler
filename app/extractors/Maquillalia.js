/**
 * Requires
 */
const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
const logger = require('logger');
const md5 = require('md5');

/**
 * Constructor
 */
var Maquillalia = function(config) {
  // Vars
  this.config = config;
  this.MAIN_URL = 'http://www.maquillalia.com';
  this.SHOP_ID = 'maquillalia';
}

/**
 * Properties
 */
Maquillalia.prototype = {  

  // For a given category page URL, extract all product urls on it
  getProductURLsFromCategoryPageURL: function(categoryPageURL, cb){
    //Vars
    var self = this;
    var producturls = [];
    //Retrieve the first one
    if (!categoryPageURL)
      cb(new Error('No categoryPageURL defined'));
    else{
      logger.debug('Request category page...');
      request(categoryPageURL, function (error, response, body) {
        logger.debug('Request OK, category page received');
        if (error)
          cb(error)
        else{
          var $;          
          try{
            //Safely tries to parse object
            $ = cheerio.load(body);
            var element = $('.libelle')
            //logger.debug('Partial page parsed, found '+element.length+' elements.');
            element.each(function(i, elem) {        
              //logger.debug('Element index ', i);      
              var rawurl = $(this).children().first().attr('href');
              var producturl = self.MAIN_URL + rawurl.split(';')[0];
              //logger.debug('Element url ', producturl);
              producturls.push(producturl);
            });
          }
          catch(e){
            error = e;
          }
          if (error)
            cb(error);
          else{
            //Build            
            cb(null, producturls);
          }
        }
      });
    }
  },

  getProductsFromProductURL: function(productURL, cb){

   //@TODO Change
    var product = {
      shop: this.MAIN_URL,
      brand: "Kat Von D",
      url: productURL,
      product: "Lock-it Makeup Setting Mist",
      detail: "Vaporizador para fijar el maquillaje",
      color: "Spray fijador de maquillage",
      price: 2000
    }
    product.product_store_id = md5(product.shop+product.product+product.brand+product.color);
    cb(null, [product]);

    //shop
    //brand
    //url
    //product
    //detail
    //color
    //price
    //store_id

    //Vars
    // var self = this;    
    // var products = [];
    // //Retrieve the first one
    // if (!productURL)
    //   cb(new Error('No productURL defined'));
    // else{
    //   logger.debug('Request product URL page...', productURL);
    //   request(productURL, function (error, response, body) {
    //     logger.debug('Request OK, product received');
    //     if (error)
    //       cb(error)
    //     else{
    //       var $;          
    //       try{
    //         //Safely tries to parse object
    //         $ = cheerio.load(body);
    //         var product = {
    //           shop : self.MAIN_URL,
    //           brand : $('.brand-label').text().trim(),          
    //         }                        
    //         var product_label = $('.product-label').text().split("\n");
    //         product.product = product_label[1].trim();
    //         if (product_label.length>2)
    //           product.detail = product_label[2].trim();

    //         //Parse colors                                
    //         $('.sku-line').each(function(i, elem){              
    //           var product_ex = JSON.parse(JSON.stringify(product));
    //           product_ex.color = $(this).find('.sku-title').text().trim();
    //           product_ex.price = parsePrice($(this).find('.price').text().trim());   
    //           product_ex.product_store_id = md5(product_ex.store+product_ex.product+product_ex.brand+product_ex.color);            
    //           //Add
    //           products.push(product_ex);
    //         });
    //       }
    //       catch(e){
    //         error = e;
    //       }
    //       if (error)
    //         cb(error);
    //       else{
    //         //Build            
    //         logger.debug('Products arr');
    //         logger.debug(products)
    //         cb(null, products);
    //       }
    //     }
    //   });
    // }    
  }

};

/**
 * Exports
 */
module.exports = Maquillalia;


/**
 * Privates
 */
function parsePrice(raw){
  return parseFloat(raw.slice(0,-1).replace(',', ''));
}

function parseProductCrawl(rawProduct){
  //shop
  //brand
  //url
  //product
  //detail
  //color
  //price
  //store_id
}