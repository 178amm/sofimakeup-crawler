
/*
 * Module dependencies.
 */
var fs = require('fs');
//var _ = require('underscore');
var pageURL = require('./models/PageURL');
var productURL = require('./models/ProductURL');
var productPrice = require('./models/ProductPrice');
var mainProducts = require('./main.products');
const path = require('path');
const logger = require('logger');
const uuidv4 = require('uuid/v4');
const async = require('async');
const mongoose = require('mongoose');
var Product = mongoose.model('Product');

/*
 * Exports
 */

// Read a category list form a json file and extract all category pages from each cat.
exports.crawlCategoryPagesFromJSON = function (parser_name, json_path, cb){

    if (!parser_name)
        cb(new Error('MAIN: Parser parser name not provided'));
    else{
        // Read File
        var jpath = json_path ? json_path : './data/'+parser_name.toLowerCase()+'.categories.json';
        var catPages = require(path.resolve(jpath));
        console.log(catPages);
        if (!catPages)
            cb(new Error('MAIN: Cant read json or empty category pages'));
        else{
            //Load pager & get urls
            var Pager = require('./pagers/'+capitalize(parser_name));
            var pager = new Pager();
            
            async.each(catPages, function(categorieURL, cba){
                pager.getCategoryPageURLsFromCategoryURL(categorieURL, function(err, data){
                    if (err)
                        cba(err);
                    else{ //Save links
                        pageURL.add(data, parser_name, cba);
                    } 
                });
            }, function(err){
                //Done! All pages processed
                cb(err);
            });
        }   
    }
}

exports.crawlProductFromCategoryPages = function(parser_name, cb){

    //Retrieve all category page urls
    if (!parser_name)
        cb(new Error('MAIN: Parser parser name not provided'));
    else
        pageURL.getAll(parser_name, function(err, pageURLs){
            if (err)
                console.log(err)
            else{ //Start crawling per page
                var Extractor = require('./extractors/'+capitalize(parser_name));
                var extractor = new Extractor();
                async.each(pageURLs, function(pageURL, cba){
                    extractor.getProductURLsFromCategoryPageURL(pageURL, function(err, data){
                        if (err)
                            cba(err);
                        else{ //Save links
                            logger.debug('Product pack link retrieved, length: ', data.length)
                            productURL.add(data, parser_name, cba);   
                        }                 
                    });
                }, function(err){
                    //Done, all pages processed
                    cb(err);
                }); 
            }        
        });
}

exports.crawlProductsPricesFromProductURLs = mainProducts.crawlProductsPricesFromProductURLs;

/*
 * Privates
 */

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

//function 