/**
 * Requires
 */
const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
const logger = require('logger');

/**
 * Constructor
 */
var Sephora = function(config) {
  // Vars
  this.config = config;
}

/**
 * Properties
 */
Sephora.prototype = {  

  // For a given main category URL (usually the first one) returns a set of urls representing all sub pages for that category.
  getCategoryPageURLsFromCategoryURL: function(categoryURL, cb){
    //Vars
    var pageurls = [];
    //Retrieve the first one
    if (!categoryURL)
      cb(new Error('No categoryURL defined'));
    else{    
      logger.debug('Starting request for retrieving category urls');
      request(categoryURL, function (error, response, body) {
          if (error)
            cb(error)
          else{
            logger.debug('Request OK! Category URL retrieved');
            var $;
            var MAX_PAGES;
            try{
              //Safely tries to parse object
              $ = cheerio.load(body);
              MAX_PAGES = parseInt($('.triPages2>a:nth-last-child(2)').html());
            }
            catch(e){
              error = e;
            }
            if (error)
              cb(error);
            else{
              //Build/Fill the pageurls object
              pageurls.push(categoryURL);
              for (var i=0; i<MAX_PAGES-1; i++){
                pageurls.push(categoryURL + '/' + (i+2));
              }
              cb(null, pageurls);
            }
          }
        });
    }
  },

};

/**
 * Exports
 */
module.exports = Sephora;


/**
 * Privates
 */